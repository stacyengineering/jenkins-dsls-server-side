from src.routes.user import User
from src.routes.info import Info


class Index(object):
    exposed = True

    def __init__(self, cont):
        self.user = User(cont)
        self.info = Info(cont)