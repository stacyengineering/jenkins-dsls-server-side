import cherrypy

from src.routes.info import Info


@cherrypy.popargs("user_id")
class User:
    exposed = True

    def __init__(self, cont):
        self._cont = cont
        self.info = Info(cont)

    @cherrypy.tools.json_out()
    def GET(self, user_id=None):
        if not user_id:
            return self._cont.models.users.get_all()
        return self._cont.models.users.get({"_id": user_id})[0]

    @cherrypy.tools.json_in()
    @cherrypy.tools.json_out()
    def POST(self):
        user_data = cherrypy.request.json
        return self._cont.models.users.save(user_data)

    def DELETE(self, user_id):
        res = self._cont.models.users.delete(user_id)
        if not res:
            cherrypy.response.status = 404
            return {"reason": "{} not found".format(user_id)}
