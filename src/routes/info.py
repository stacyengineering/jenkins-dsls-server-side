import cherrypy


@cherrypy.popargs("info_id")
class Info:
    exposed = True

    def __init__(self, cont):
        self.__cont = cont

    @cherrypy.tools.json_out()
    def GET(self, user_id=None, info_id=None):
        if info_id and user_id:
            return self.__cont.models.info.get({
                "_id": info_id,
                "user_id": user_id
            })
        if info_id:
            return self.__cont.models.info.get({"_id": info_id})[0]
        if user_id:
            return self.__cont.models.info.get({"user_id": user_id})
        return self.__cont.models.info.get_all()

    @cherrypy.tools.json_in()
    @cherrypy.tools.json_out()
    def POST(self, user_id):
        data = cherrypy.request.json
        data['user_id'] = user_id
        return self.__cont.models.info.save(data)