import logging

import pymongo
from bson.objectid import ObjectId

from src.db_tools.db_helpers import stringify_id


class DBAcessor(object):

    def __init__(self, db_name, db_addr):
        self.client = pymongo.MongoClient(db_addr)
        self.db = self.client[db_name]
        self.collect = ""
        self.what = {}
        self.result = {}
        self._log = logging.getLogger(__file__)

    def collection(self, name):
        """
        Collection name

        :param name: str, name of collection you want to operate on
        :return: obj, instance of this class
        """
        self.collect = name
        return self

    def item(self, what):
        """
        Item you want to find or operate on.

        Examples:
            {"_id": "1283ahdg8d1d"}
            {"email": "john.smith@gmail.com"}

        :param what: dict, item you would like to find or operate on
        :return: obj, instance of this class
        """
        if what.get("_id"):
            if not isinstance(what['_id'], ObjectId):
                what['_id'] = ObjectId(what['_id'])
        self.what = what
        return self

    @stringify_id
    def find_all(self):
        self._log.debug("Searching in collection: %s" % self.collect)
        return list(getattr(self.db, self.collect).find(self.what))

    @stringify_id
    def find(self):
        """
        Find one object in given db for given collection and given item
        traits.

        Usage example:
            db = DBAccessor("mydb")
            db.collection("users").item("_id": "1827dagh").find()

        :return: dict or None, what was found
        """
        self._log.debug("Searching in collection: %s" % self.collect)
        return getattr(self.db, self.collect).find_one(self.what)

    def save(self, content):
        self._log.debug("Saving {content} to collection: {collect}".format(
            content=content, collect=self.collect))
        self.result = getattr(self.db, self.collect).insert_one(
            content).inserted_id
        return self

    def delete(self):
        self._log.debug("Deleting item: {item} from {collect}".format(
            item=self.what, collect=self.collect))
        getattr(self.db, self.collect).delete_many(self.what)
        return self

    def count(self):
        """
        Count all documents in given collection
        :return: number of documents
        """
        return getattr(self.db, self.collect).count()
