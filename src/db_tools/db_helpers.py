
def stringify_id(func):
    def wrapper(*args, **kwargs):
        result = func(*args, **kwargs)
        if result:
            if type(result) is list:
                for result_item in result:
                    result_item["_id"] = str(result_item['_id'])
            else:
                result["_id"] = str(result["_id"])
        return result
    return wrapper
