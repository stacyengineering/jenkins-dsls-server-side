import json
import os

from src.db_tools.db_accessor import DBAcessor
from src.models.user import UserModel
from src.models.info import InfoModel


class Models:
    def __init__(self, db_handler):
        self.users = UserModel(db_handler)
        self.info = InfoModel(db_handler)


class Context(object):
    def __init__(self):
        """
        Configuration manager.
        """
        config = "{}/configuration/dev_config.json".format(
            os.path.dirname(os.path.dirname(__file__)))

        with open(config) as fl:
            self.settings = json.loads(fl.read())
            self.host_name = self.settings["host_name"]
            self.port_num = self.settings["port_num"]
            db_accessor = DBAcessor(self.settings['db_name'], self.settings['db_addr'])
            self.models = Models(db_accessor)
            # self.jenkins_url = self.settings['jenkins_url']
            # self.jenkins_username = self.settings['jenkins_username']
            # self.jenkins_password = self.settings['jenkins_password']

