from src.models.base import ModelsBase


class UserModel(ModelsBase):

    @property
    def collection(self):
        return self._db.collection("users")
