from src.models.base import ModelsBase


class InfoModel(ModelsBase):

    @property
    def collection(self):
        return self._db.collection("info")
