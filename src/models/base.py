

class ModelsBase:
    def __init__(self, db_handler):
        self._db = db_handler

    def _stringify_id(self, cursor):
        result = []
        for current_user in cursor:
            obj_id = str(current_user.pop('_id'))
            result.append({
                obj_id: current_user
            })
        return result

    @property
    def collection(self):
        raise Exception("Collection was not specified!")

    def save(self, data):
        return {"id": str(self.collection.save(data).result)}

    def delete(self, obj_id):
        ref = self.collection.item({"_id": obj_id})
        if not ref.find():
            return False
        ref.delete()
        return True

    def get(self, obj_props):
        cursor = self.collection.item(obj_props).find_all()
        return self._stringify_id(cursor)

    def get_all(self):
        return self.get({})
