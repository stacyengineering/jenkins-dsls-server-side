from unittest.mock import Mock
from src.models.info import InfoModel

from tests.test_base import TestBase


class TestModelsUser(TestBase):

    def test_init(self):
        db_handler = Mock()
        models_user = InfoModel(db_handler)
        self.assertEqual(models_user._db, db_handler)

    def test_collection(self):
        db_handler = Mock()
        models_user = InfoModel(db_handler)
        self.assertTrue(models_user.collection)

    def test_save(self):
        db_handler = Mock()
        models_user = InfoModel(db_handler)
        res = models_user.save({"username": "test", "surname": "testowicz"})
        self.assertEqual(models_user.collection.save.call_count, 1)
        self.assertTrue("id" in res)

    def test_delete(self):
        db_handler = Mock()
        models_user = InfoModel(db_handler)
        models_user.delete("12345")
        self.assertEqual(models_user.collection.item.call_count, 1)
        self.assertTrue("().find" in models_user.collection.item.mock_calls[1][0],
                        "Verify find was called")
        self.assertTrue("().delete" in models_user.collection.item.mock_calls[2][0],
                        "Verify delete was called")

    def test_get(self):
        db_handler = Mock()
        models_user = InfoModel(db_handler)
        models_user._stringify_id = Mock()
        props = {"id": "user-id-12345"}
        models_user.get(props)
        self.assertEqual(models_user.collection.item.call_count, 1)
        self.assertEqual(models_user.collection.item.call_args[0][0], props)
        self.assertTrue("().find_all" in models_user.collection.item.mock_calls[1][0],
                        "Verify find_all was called")
        self.assertEqual(models_user._stringify_id.call_count, 1)

    def test_get_all(self):
        db_handler = Mock()
        models_user = InfoModel(db_handler)
        models_user._stringify_id = Mock()
        models_user.get_all()
        self.assertEqual(models_user.collection.item.call_args[0][0], {})
        self.assertEqual(models_user._stringify_id.call_count, 1)
