from unittest.mock import Mock, patch, PropertyMock
from src.models.base import ModelsBase

from tests.test_base import TestBase


class TestModelsBase(TestBase):

    def test_init(self):
        db_handler = Mock()
        models_base = ModelsBase(db_handler)
        self.assertEqual(models_base._db, db_handler)

    def test_stringify_id(self):
        db_handler = Mock()
        models_base = ModelsBase(db_handler)
        user_id = "id-of-user-1234"
        cursor = [{"_id": user_id, "some_data": "1235"}]
        res = models_base._stringify_id(cursor)
        self.assertTrue(res, [{user_id: {"some_data": "1235"}}])

    def test_stringify_id_empty_cursor(self):
        db_handler = Mock()
        models_base = ModelsBase(db_handler)
        cursor = []
        res = models_base._stringify_id(cursor)
        self.assertEqual(res, [])

    def test_collection(self):
        db_handler = Mock()
        models_base = ModelsBase(db_handler)
        with self.assertRaises(Exception) as context:
            models_base.collection
            self.assertEqual(context, "Collection was not specified!")
