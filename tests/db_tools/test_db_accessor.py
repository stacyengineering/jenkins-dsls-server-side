from unittest.mock import Mock
from src.db_tools import db_accessor

from tests.test_base import TestBase


class TestDbAccessor(TestBase):

    @classmethod
    def setUpClass(cls):
        db_accessor.ObjectId = Mock
        db_accessor.db_tools = Mock()
        db_accessor.logging = Mock()
        db_accessor.pymongo.MongoClient = Mock(
            return_value={"db_name": Mock()})

    def test_init(self):
        db = db_accessor.DBAcessor("db_name", "b")
        self.assertTrue(hasattr(db, "client"))
        self.assertTrue(hasattr(db, "db"))
        self.assertTrue(hasattr(db, "collect"))
        self.assertTrue(hasattr(db, "what"))
        self.assertTrue(hasattr(db, "result"))
        self.assertTrue(hasattr(db, "_log"))

    def test_collection(self):
        collection_name = "collection_name"
        db = db_accessor.DBAcessor("db_name", "b")
        res = db.collection(collection_name)
        self.assertTrue(isinstance(res, db_accessor.DBAcessor))
        self.assertEqual(db.collect, collection_name)

    def test_item_object_id(self):
        db = db_accessor.DBAcessor("db_name", "b")
        what = {"_id": db_accessor.ObjectId()}
        # this is an integration test:
        # db.collection(collection_name).item()
        db.item(what)
        self.assertEqual(db.what, what)

    def test_item_not_object_id(self):
        db = db_accessor.DBAcessor("db_name", "b")
        what = {"_id": "other type"}
        db.item(what)
        self.assertNotEqual(db, what)

    def prepare_mocks(self):
        db = db_accessor.DBAcessor("db_name", "b")
        db.what = Mock()
        db.db = Mock()
        db.db.collection_name = Mock()
        return db

    def test_find_all_what_is_list(self):
        db = self.prepare_mocks()
        db.db.collection_name.find = Mock(return_value=[{"_id": 1}])
        db.collect = "collection_name"
        result = db.find_all()
        self.assertTrue(result)
        self.assertTrue(isinstance(result[0]['_id'], str))

    def test_find_all_what_is_empty(self):
        db = self.prepare_mocks()
        db.db.collection_name.find = Mock(return_value={})
        db.collect = "collection_name"
        result = db.find_all()
        self.assertFalse(result)

    def test_find_what_is_single_value(self):
        db = self.prepare_mocks()
        db.db.collection_name.find_one = Mock(return_value={"_id": 1})
        db.collect = "collection_name"
        result = db.find()
        self.assertTrue(result)
        self.assertTrue(isinstance(result['_id'], str))
        self.assertEqual(db.db.collection_name.find_one.call_count, 1)
        self.assertEqual(db.db.collection_name.find_one.call_args[0][0],
                         db.what)

    def test_find_what_is_empty(self):
        db = self.prepare_mocks()
        db.db.collection_name.find_one = Mock(return_value={})
        db.collect = "collection_name"
        result = db.find()
        self.assertFalse(result)
        self.assertEqual(db.db.collection_name.find_one.call_count, 1)

    def test_save(self):
        db = self.prepare_mocks()
        db.db.collection_name.insert_one = Mock()

        res = db.save({"a": "b"})
        self.assertTrue(isinstance(res, db_accessor.DBAcessor))
        self.assertEqual(getattr(db.db, db.collect).insert_one.call_count, 1)

    def test_delete(self):
        db = self.prepare_mocks()

        res = db.delete()
        self.assertTrue(isinstance(res, db_accessor.DBAcessor))
        self.assertEqual(getattr(db.db, db.collect).delete_many.call_count, 1)

    def test_count(self):
        db = self.prepare_mocks()

        res = db.count()
        self.assertEqual(getattr(db.db, db.collect).count.call_count, 1)
