from unittest.mock import Mock

from src.routes import user
from tests.test_base import TestBase


class TestUserRoute(TestBase):
    def setUp(self):
        user.Info = Mock()
        user.cherrypy = Mock()

    def test_is_exposed(self):
        self.assertEqual(user.User.exposed, True)

    def test_init(self):
        test_context = {"test": "context"}
        u_obj = user.User(test_context)
        self.assertEqual(u_obj._cont, test_context)
        self.assertTrue(hasattr(u_obj, "info"))
        self.assertTrue(hasattr(u_obj, "exposed"))

    def test_get(self):
        test_context = {"test": "context"}
        u_obj = user.User(test_context)

