from unittest.mock import Mock

from src.routes import index
from tests.test_base import TestBase


class TestIndexRoute(TestBase):
    def test_init(self):
        test_context = {"test": "context"}
        index.User = Mock()
        index.Info = Mock()
        ind = index.Index(test_context)
        self.assertTrue(hasattr(ind, "user"))
        self.assertTrue(hasattr(ind, "info"))
        self.assertEqual(index.User.call_count, 1)
        self.assertEqual(index.User.call_args[0][0], test_context)
        self.assertEqual(index.Info.call_count, 1)
        self.assertEqual(index.Info.call_args[0][0], test_context)
